package com.somospnt.techiepropagacion.service;

import com.somospnt.techiepropagacion.domain.Persona;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class PersonaServiceTest {
    
    @Autowired
    private PersonaService personaService;
    
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @Test
    public void guardar_conPersonaValida_guardaEnBaseDeDatos() {
        Persona persona = new Persona();
        persona.setId(1L);
        persona.setNombre("Jorge");
        personaService.guardar(persona);
        assertEquals(1L, JdbcTestUtils.countRowsInTable(jdbcTemplate, "persona"));
    }

}
