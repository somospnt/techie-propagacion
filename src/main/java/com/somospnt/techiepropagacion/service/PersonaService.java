package com.somospnt.techiepropagacion.service;

import com.somospnt.techiepropagacion.domain.Persona;


public interface PersonaService {

    void guardar(Persona persona);
}
