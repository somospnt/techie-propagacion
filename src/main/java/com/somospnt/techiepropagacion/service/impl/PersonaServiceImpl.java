package com.somospnt.techiepropagacion.service.impl;

import com.somospnt.techiepropagacion.domain.Persona;
import com.somospnt.techiepropagacion.repository.PersonaRepository;
import com.somospnt.techiepropagacion.service.PersonaService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PersonaServiceImpl implements PersonaService {

    private final PersonaRepository personaRepository;

    public PersonaServiceImpl(PersonaRepository personaRepository) {
        this.personaRepository = personaRepository;
    }
    
    @Override
    public void guardar(Persona persona) {
        personaRepository.save(persona);
    }
    
    
}
