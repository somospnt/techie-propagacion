package com.somospnt.techiepropagacion.repository;

import com.somospnt.techiepropagacion.domain.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonaRepository extends JpaRepository<Persona, Long>{

}
