package com.somospnt.techiepropagacion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechiePropagacionApplication {

    public static void main(String[] args) {
        SpringApplication.run(TechiePropagacionApplication.class, args);
    }
}
